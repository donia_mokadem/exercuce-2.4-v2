package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.WalletDeserializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.Set;

/**
 * DAO simple pour lecture/�criture d'un badge dans un wallet � badges digitaux multiples
 * et PAR ACCES DIRECT, donc prenant en compte les m�tadonn�es de chaque badges.
 */
public class DirectAccessBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(DirectAccessBadgeWalletDAOImpl.class);

    private final File walletDatabase;

    /**
     * Constructeur �l�mentaire
     *
     * @param dbPath
     * @throws IOException
     */
    public DirectAccessBadgeWalletDAOImpl(String dbPath) throws IOException{
        this.walletDatabase = new File(dbPath);
    }

    /**
     * Permet d'ajouter le badge au Wallet
     * @deprecated Sur les nouveaux formats de base CVS, Utiliser de pr�f�rence addBadge(DigitalBadge badge)
     * @hidden
     * @param image
     * @throws IOException
     */
    @Override
    public void addBadge(File image) throws IOException{
        LOG.error("Non support�");
        throw new IOException("M�thode non support�e, utilisez plut�t addBadge(DigitalBadge badge)");
    }

    /**
     * Permet d'ajouter le badge au nouveau format de Wallet
     *
     * @param badge
     * @throws IOException
     */
    @Override
    public void addBadge(DigitalBadge badge) throws IOException{
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))){
            ImageStreamingSerializer serializer = new WalletSerializerDirectAccessImpl();
            serializer.serialize(badge, media);
        }
    }

    /**
     * Permet de r�cup�rer le badge du Wallet
     * @deprecated Sur les nouveaux formats de base CVS, Utiliser de pr�f�rence getBadgeFromMetadata
     *
     * @param imageStream
     * @throws IOException
     */
    @Override
    public void getBadge(OutputStream imageStream) throws IOException{
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
        }
    }


    /**
     * {@inheritDoc}
     * @return List<DigitalBadgeMetadata>
     */
    @Override
    public Set<DigitalBadge> getWalletMetadata() throws IOException {
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            return new MetadataDeserializerDatabaseImpl().deserialize(media);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta) throws IOException {
        Set<DigitalBadge> metas = this.getWalletMetadata();
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            new WalletDeserializerDirectAccessImpl(imageStream, metas).deserialize(media, meta);
        }
    }

	@Override
	public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException {
		// TODO Auto-generated method stub
		
	}


}
