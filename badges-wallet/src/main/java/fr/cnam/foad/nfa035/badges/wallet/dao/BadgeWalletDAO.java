package fr.cnam.foad.nfa035.badges.wallet.dao;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

/**
 * Interface définissant le comportement élémentaire d'un DAO destinée à la gestion de badges digitaux
 */
public interface BadgeWalletDAO {

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param badge3
     * @throws IOException
     */
    void addBadge(DigitalBadge badge3) throws IOException;

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    void getBadge(OutputStream imageStream) throws IOException;

	/**
	 * {@inheritDoc}
	 *
	 * @param imageStream
	 * @param expectedBadge3
	 * @throws IOException
	 */
	void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge expectedBadge3) throws IOException;

	/**
	 * Permet d'ajouter le badge au Wallet
	 * @deprecated Sur les nouveaux formats de base CVS, Utiliser de pr�f�rence addBadge(DigitalBadge badge)
	 * @hidden
	 * @param image
	 * @throws IOException
	 */
	void addBadge(File image) throws IOException;

}
